package ru.t1.semikolenov.tm.exception.field;

public final class DateBeginIncorrectException extends AbstractFieldException {

    public DateBeginIncorrectException() {
        super("Error! Data begin is incorrect...");
    }

}
