package ru.t1.semikolenov.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
