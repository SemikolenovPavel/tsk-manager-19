package ru.t1.semikolenov.tm.exception.field;

public class EmptyNameException extends AbstractFieldException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
