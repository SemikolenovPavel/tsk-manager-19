package ru.t1.semikolenov.tm.command.user;

import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "Log in.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN SYSTEM]");
        System.out.println("ENTER LOGIN:");
        String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
