package ru.t1.semikolenov.tm.command.system;

import ru.t1.semikolenov.tm.api.service.ICommandService;
import ru.t1.semikolenov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
