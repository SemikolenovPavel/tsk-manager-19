package ru.t1.semikolenov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Log out.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT SYSTEM]");
        getAuthService().logout();
    }

}
