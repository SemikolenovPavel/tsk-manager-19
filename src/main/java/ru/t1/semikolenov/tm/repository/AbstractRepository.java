package ru.t1.semikolenov.tm.repository;

import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.model.AbstractModel;
import ru.t1.semikolenov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) == null;
    }

    @Override
    public M findOneById(String id) {
        for (final M model: models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(Integer index) {
        return models.get(index);
    }

    @Override
    public int getSize() {
        if (models == null || models.isEmpty()) return 0;
        return models.size();
    }

    @Override
    public M remove(M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }
}
