package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task>{

    List<Task> findAllByProjectId(String projectId);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task updateById(String id, String name, String description);

    Task updateById(String id, String name, String description, Date dateBegin, Date dateEnd);

    Task updateByIndex(Integer index, String name, String description);

    Task updateByIndex(Integer index, String name, String description, Date dateBegin, Date dateEnd);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
