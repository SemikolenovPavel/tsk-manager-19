package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    List<Task> findAllByProjectId(String projectId);

    Task create(String name);

    Task create(String name, String description);

}
