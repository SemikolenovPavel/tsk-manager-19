package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

}
