package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project>{

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String id, String name, String description);

    Project updateById(String id, String name, String description, Date dateBegin, Date dateEnd);

    Project updateByIndex(Integer index, String name, String description);

    Project updateByIndex(Integer index, String name, String description, Date dateBegin, Date dateEnd);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
