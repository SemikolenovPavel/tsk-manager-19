package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.repository.TaskRepository;

import java.util.*;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Task create(String name, String description, Date dateBegin, Date dateEnd) {
        final Task task = repository.create(name, description);
        if (task == null) throw new TaskNotFoundException();
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else task.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
