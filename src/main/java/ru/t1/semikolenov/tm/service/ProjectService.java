package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.repository.AbstractRepository;
import ru.t1.semikolenov.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(String name, String description, Date dateBegin, Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
